import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class TarascaTokenGenerator {
    private static final HashMap<String, String> applicationResult = new HashMap<>();

    static private MessageDigest messageDigest512;

    static {
        try {
            messageDigest512 = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static class Configuration {
        private String secretString = "0000000000000000000000000000000000000000000000000000000000000000";

        private String seriesString = "Redeem Assets 2021";
        private int indexLength = 16;
        private int signatureLength = 16;

        private String fileName = "configuration.json";
        private String codeFileName = "tokens.json";

        private long codeQuantity = 10;
    }

    private static final Configuration configuration = new Configuration();

    private static class Metadata {
        private byte[] magic = new byte[0x04];
        private long assetQuantityQNT = 3;
        private int size = 0;

        public int calculateSize(int indexLength, int signatureLength) {
            return magic.length + 0x08 + indexLength + signatureLength;
        }
    }

    private static final Metadata metadata = new Metadata();

    public static void main(String[] args) throws Exception {

        if (args.length != 1 && args.length != 3) {
            applicationResult.put("errorDescription", "usage :\nw output configuration\nr input configuration\n");
        } else {

            if(args.length > 1) {
                configuration.codeFileName = args[1];
            }

            if(args.length > 2) {
                configuration.fileName = args[2];
            }

            configurationRead(configuration.fileName);

            if (args[0].equals("w")) {
                try {
                    codeWriteFile(configuration.codeFileName);

                } catch (Exception e) {
                    e.printStackTrace();
                    applicationResult.put("errorDescription", "error generating codes");
                }

            } else if (args[0].equals("r")) {

                try {
                    codeReadFile(configuration.codeFileName);

                } catch (Exception e) {
                    e.printStackTrace();
                    applicationResult.put("errorDescription", "error verifying codes");
                }
            }
        }

        JSONObject applicationResultJson = new JSONObject();

        for(String key : applicationResult.keySet()) {
         applicationResultJson.put(key, applicationResult.get(key));
        }

        System.out.println(applicationResultJson.toJSONString());

        System.exit(0);
    }

    private static void configurationRead(String filePath) throws IOException, ParseException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            applicationResult.put("errorDescription", "empty configuration : " + filePath);
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));

        if(jsonConfiguration.containsKey("code")) {
            JSONObject code = (JSONObject) jsonConfiguration.get("code");

            if(code.containsKey("series")) {
                configuration.seriesString = (String) code.get("series");
            }

            if(code.containsKey("secret")) {
                configuration.secretString = (String) code.get("secret");
            }

            if(code.containsKey("indexLength")) {
                configuration.indexLength = (int)(long) code.get("indexLength");
            }

            if(code.containsKey("signatureLength")) {
                configuration.signatureLength = (int)(long) code.get("signatureLength");
            }

            if(code.containsKey("quantityQNT")) {
                metadata.assetQuantityQNT = Long.parseUnsignedLong((String) code.get("quantityQNT"));
            }

            if(code.containsKey("magic")) {
                metadata.magic = bytesFromHexString((String) code.get("magic"));
            }

            if(code.containsKey("codeQuantity")) {
                configuration.codeQuantity = (long) code.get("codeQuantity");
            }
        }

        metadata.size = metadata.calculateSize(configuration.indexLength, configuration.signatureLength);
    }

    private static void codeWriteFile(String filePath) throws IOException, CloneNotSupportedException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("indexLength", configuration.indexLength);
        jsonObject.put("signatureLength", configuration.signatureLength);

        JSONArray jsonArray = new JSONArray();

        jsonObject.put("tokens", jsonArray);

        byte[] byteArray = codeBytesArray(configuration.indexLength, configuration.signatureLength, metadata.magic, metadata.assetQuantityQNT, configuration.secretString, configuration.seriesString, configuration.codeQuantity);
        int codeSize = metadata.size;

        int codeCount = byteArray.length / codeSize;

        byte[] codeObject = new byte[codeSize];

        for(int i = 0; i < codeCount; i++) {
            System.arraycopy(byteArray, i * codeSize, codeObject, 0, codeSize);
            jsonArray.add(hexStringFromBytes(codeObject));
        }

        Files.write(file.toPath(), jsonObject.toJSONString().getBytes());
    }

    private static byte[] codeBytesArray(int indexLength, int signatureLength, byte[] magic, long quantityQNT, String secretString, String seriesString, long arraySize) throws IOException, CloneNotSupportedException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        messageDigest512.update(secretString.getBytes(StandardCharsets.UTF_8));
        messageDigest512.update(seriesString.getBytes(StandardCharsets.UTF_8));

        for(long i = 0; i < arraySize; i++) {
            MessageDigest messageDigest = (MessageDigest) messageDigest512.clone();
            byteArrayOutputStream.write(codeBytes(indexLength, signatureLength, magic, quantityQNT, messageDigest));
        }

        return byteArrayOutputStream.toByteArray();
    }

    private static byte[] codeBytes(int indexLength, int signatureLength, byte[] magic, long quantityQNT, MessageDigest messageDigest) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        // format : (magic[magic.length] : quantityQNT[0x08] : index[indexLength]) : signature[signatureLength]

        byteArrayOutputStream.write(magic);

        byteArrayOutputStream.write(ByteBuffer.allocate(0x08).order(ByteOrder.BIG_ENDIAN).putLong(quantityQNT).array());

        Random random = new Random();

        byte[] indexBytes = new byte[indexLength];

        random.nextBytes(indexBytes);

        byteArrayOutputStream.write(indexBytes);

        byte[] message = byteArrayOutputStream.toByteArray();

        byte[] signature512 = messageDigest.digest(message);
        byte[] signature = new byte[signatureLength];

        System.arraycopy(signature512, 0, signature, 0, signatureLength);

        byteArrayOutputStream.write(signature);

        return byteArrayOutputStream.toByteArray();
    }

    private static void codeReadFile(String filePath) throws IOException, ParseException, CloneNotSupportedException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            applicationResult.put("errorDescription", "empty file : " + filePath);
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));


        if(!jsonObject.containsKey("tokens")) {
            applicationResult.put("errorDescription", "contains no tokens : " + filePath);
            return;
        }

        JSONArray jsonArray = (JSONArray) jsonObject.get("tokens");

        messageDigest512.update(configuration.secretString.getBytes(StandardCharsets.UTF_8));
        messageDigest512.update(configuration.seriesString.getBytes(StandardCharsets.UTF_8));

        for(int i = 0; i < jsonArray.size(); i++) {
            MessageDigest messageDigest = (MessageDigest) messageDigest512.clone();
            byte[] codeBytes = bytesFromHexString((String) jsonArray.get(i));

            boolean isValid = codeVerifyBytes(codeBytes, messageDigest);

            if(!isValid) {
                applicationResult.put("errorDescription", "found invalid token # : " + i);
                applicationResult.put("tokenBytes", hexStringFromBytes(codeBytes));
                return;
            }
        }

        if(jsonArray.size() != 0) {
            applicationResult.put("tokenCount", Integer.toString(jsonArray.size()));
            applicationResult.put("valid", "true");
        }
    }

    private static boolean codeVerifyBytes(byte[] bytes, MessageDigest messageDigest) {
        if(bytes == null || bytes.length < metadata.size) {
            return false;
        }

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        // format : (magic[magic.length] : quantityQNT[0x08] : index[indexLength]) : signature[signatureLength]

        int bytesRead = 0;

        byte[] magic = new byte[metadata.magic.length];
        byte[] quantityQNTBytes = new byte[0x08];
        byte[] indexBytes = new byte[configuration.indexLength];
        byte[] signatureBytes = new byte[configuration.signatureLength];

        bytesRead += byteArrayInputStream.read(magic, 0x00, magic.length);
        bytesRead += byteArrayInputStream.read(quantityQNTBytes, 0x00, quantityQNTBytes.length);
        bytesRead += byteArrayInputStream.read(indexBytes, 0x00, indexBytes.length);
        bytesRead += byteArrayInputStream.read(signatureBytes, 0x00, signatureBytes.length);

        if(bytesRead != metadata.size) {
            return false;
        }

        byte[] signatureSource = new byte[configuration.signatureLength];
        byte[] codeBytes = new byte[metadata.size - signatureSource.length];

        System.arraycopy(bytes, 0, codeBytes, 0, codeBytes.length);
        System.arraycopy(bytes, codeBytes.length, signatureSource, 0, signatureSource.length);

        byte[] signature512 = messageDigest.digest(codeBytes);
        byte[] signature = new byte[signatureBytes.length];

        System.arraycopy(signature512, 0, signature, 0, signature.length);

        return Arrays.equals(signatureSource, signature);
    }

    public static String hexStringFromBytes(byte[] bytes) {
        return String.format("%0" + (bytes.length << 1) + "x", new BigInteger(1, bytes));
    }

    public static byte[] bytesFromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
