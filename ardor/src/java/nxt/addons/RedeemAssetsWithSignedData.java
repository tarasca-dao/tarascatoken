package nxt.addons;

import nxt.db.DbUtils;
import nxt.dbschema.Db;

import nxt.Nxt;
import nxt.NxtException;
import nxt.crypto.Crypto;

import nxt.http.APIServlet;
import nxt.http.APITag;
import nxt.http.ParameterParser;
import nxt.http.callers.GetAssetCall;

import nxt.util.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;
import org.json.simple.parser.JSONParser;

import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import java.math.BigInteger;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

import nxt.util.Convert;
import org.json.simple.parser.ParseException;


public final class RedeemAssetsWithSignedData implements AddOn {


    public APIServlet.APIRequestHandler getAPIRequestHandler() {
        return new RedeemAssetsWithSignedDataAPI(new APITag[]{APITag.ADDONS}, "data", "recipient", "broadcast");
    }

    public String getAPIRequestType() {
        return "redeemAssetsWithSignedData";
    }

    private static class CodeConfiguration {
        public byte[] privateKey = new byte[0x20];
        public String secretString = "0000000000000000000000000000000000000000000000000000000000000000";

        public String seriesString = "Redeem Assets 2021";
        public int indexLength = 16;
        public int signatureLength = 16;

        public TreeMap<Long, Long> assetIdWeightMap = null;

        public int assetTransferChain = 2;

        public String secretForInvalidationString = "0";

        public byte[] magic = new byte[0x04];

        public int calculateSize() {
            return magic.length + 0x08 + indexLength + signatureLength;
        }
    }

    private static final List<CodeConfiguration> codeConfigurationList = new ArrayList<>();

    private static class Configuration {
        String adminPasswordString = null;

        private final String fileName = "conf/processes/RedeemAssetsWithSignedData.json";

        String secretForRandomString = "0";
        String secretForRandomSerialString = "0";

        String feePriorityString = "NORMAL";
        long minFeeRateNQTPerFXT = 375000000;
        long maxFeeRateNQTPerFXT = 500000000;

        long feeRateNQTPerFXT = minFeeRateNQTPerFXT;

        public int assetTransferChain = 2;

        CodeConfiguration codeConfiguration = null;
    }

    private static final Configuration configuration = new Configuration();

    private static final TreeMap<Long, Integer> assetIssueQuantityCache =  new TreeMap<>();
    private static final TreeMap<Long, Integer> assetDecimalCache =  new TreeMap<>();
    private static final SortedSet<String> messageHashCache= new TreeSet<>();

    static SecureRandom random;

    @Override
    public void init() {
        File file;

        try {
            file = new File(configuration.fileName);
        } catch (Exception e) {
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration = null;

        try {
            jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        assert jsonConfiguration != null;

        configuration.adminPasswordString = JsonSimpleFunctions.getString(jsonConfiguration, "adminPassword", configuration.adminPasswordString);

        configuration.minFeeRateNQTPerFXT = JsonSimpleFunctions.getLongFromStringUnsigned(jsonConfiguration, "minFeeRateNQTPerFXT", configuration.minFeeRateNQTPerFXT);
        configuration.maxFeeRateNQTPerFXT = JsonSimpleFunctions.getLongFromStringUnsigned(jsonConfiguration, "maxFeeRateNQTPerFXT", configuration.maxFeeRateNQTPerFXT);


        configuration.secretForRandomString = JsonSimpleFunctions.getString(jsonConfiguration, "secretForRandomString", configuration.secretForRandomString);
        configuration.feePriorityString = JsonSimpleFunctions.getString(jsonConfiguration, "feePriority", configuration.feePriorityString).toUpperCase();

        configuration.secretForRandomString = JsonSimpleFunctions.getString(jsonConfiguration, "secretForRandomString", configuration.secretForRandomString);
        configuration.secretForRandomSerialString = JsonSimpleFunctions.getString(jsonConfiguration, "secretForRandomSerialString", configuration.secretForRandomSerialString);

        if(jsonConfiguration.containsKey("codeConfiguration")) {
            JSONArray codeConfigurationArray = (JSONArray) jsonConfiguration.get("codeConfiguration");

            for(Object object : codeConfigurationArray) {
                JSONObject codeConfigurationJsonObject = (JSONObject) object;

                CodeConfiguration codeConfiguration = new CodeConfiguration();

                codeConfiguration.privateKey = JsonSimpleFunctions.getBytesFromHexString(codeConfigurationJsonObject, "privateKey", codeConfiguration.privateKey);
                codeConfiguration.secretForInvalidationString = JsonSimpleFunctions.getString(codeConfigurationJsonObject, "secretForInvalidationString", codeConfiguration.secretForInvalidationString);

                codeConfiguration.indexLength = JsonSimpleFunctions.getInt(codeConfigurationJsonObject, "indexLength", codeConfiguration.indexLength);
                codeConfiguration.signatureLength = JsonSimpleFunctions.getInt(codeConfigurationJsonObject, "signatureLength", codeConfiguration.signatureLength);

                codeConfiguration.secretString = JsonSimpleFunctions.getString(codeConfigurationJsonObject, "secretString", codeConfiguration.secretString);
                codeConfiguration.seriesString = JsonSimpleFunctions.getString(codeConfigurationJsonObject, "seriesString", codeConfiguration.seriesString);

                codeConfiguration.magic = JsonSimpleFunctions.getBytesFromHexString(codeConfigurationJsonObject, "magic", codeConfiguration.magic);

                codeConfiguration.assetTransferChain = JsonSimpleFunctions.getInt(codeConfigurationJsonObject, "chain", codeConfiguration.assetTransferChain);

                if(codeConfigurationJsonObject.containsKey("assetIds")) {
                    JSONArray jsonValidAssetArray = (JSONArray) codeConfigurationJsonObject.get("assetIds");
                    codeConfiguration.assetIdWeightMap = JsonSimpleFunctions.mapLongLongFromJsonStringArray(jsonValidAssetArray);
                }

                codeConfigurationList.add(codeConfiguration);
            }
        }
    }

    public static class RedeemAssetsWithSignedDataAPI extends APIServlet.APIRequestHandler {
        private static final TreeMap<String, Integer> invalidationCache =  new TreeMap<>();

        private JSONObject applicationResponse;

        private RedeemAssetsWithSignedDataAPI(APITag[] apiTags, String... origParameters) {
            super(apiTags, origParameters);
        }

        @Override
        protected JSONStreamAware processRequest(HttpServletRequest req) throws NxtException {
            applicationResponse = new JSONObject();

            MessageDigest messageDigestForMAC;
            MessageDigest messageDigest256;

            try {
                messageDigestForMAC = MessageDigest.getInstance("SHA-512");
                messageDigest256 = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                applicationResponse.put("isValid", false);
                applicationResponse.put("errorDescription", "missing hash function : SHA-256 or SHA-512");
                return applicationResponse;
            }

            boolean broadcast = "true".equalsIgnoreCase(req.getParameter("broadcast"));

            long recipient = ParameterParser.getAccountId(req,"recipient", false);

            byte[] dataCombinedBytes = ParameterParser.getBytes(req, "data", true);

            prepareRandom(dataCombinedBytes);

            long numberOfPicks = -1;

            for(CodeConfiguration codeConfigurationCandidate : codeConfigurationList) {
                messageDigestForMAC.update(codeConfigurationCandidate.secretString.getBytes(StandardCharsets.UTF_8));
                messageDigestForMAC.update(codeConfigurationCandidate.seriesString.getBytes(StandardCharsets.UTF_8));

                numberOfPicks = codeVerifyBytes(dataCombinedBytes, codeConfigurationCandidate, messageDigestForMAC);

                if(numberOfPicks <= 0) {
                    continue;
                }

                configuration.codeConfiguration = codeConfigurationCandidate;
            }

            if(configuration.codeConfiguration == null) {
                applicationResponse.put("isValid", false);
                applicationResponse.put("errorDescription",  " invalid signature");
                return applicationResponse;
            }

            TreeMap<Long, Double> assetMap = new TreeMap<>();
            double assetTotal = assetCumulativeWeightListAppend(assetMap, configuration.codeConfiguration.assetIdWeightMap);

            Logger.logInfoMessage("assets : " +  assetTotal + " : " + assetMap.size());

            if(assetTotal == 0 || numberOfPicks < 1) {
                return applicationResponse;
            }

            configuration.assetTransferChain = configuration.codeConfiguration.assetTransferChain;

            updateConfigurationFeeRate(configuration.assetTransferChain);

            List<Double> assetProbability = new ArrayList<>();
            List<Long> assetList = new ArrayList<>();

            generateCumulativeProbabilityFromAssetMap(assetProbability, assetList, assetMap, assetTotal);

            messageDigest256.reset();
            messageDigest256.update(dataCombinedBytes);

            if(configuration.codeConfiguration.secretForInvalidationString != null)
                messageDigest256.update(configuration.codeConfiguration.secretForInvalidationString.getBytes(StandardCharsets.UTF_8));

            byte[] invalidationCodeBytes = messageDigest256.digest();
            String invalidationCodeHexString = Convert.toHexString(invalidationCodeBytes);

            byte[] hashOfMessage = messageHash(invalidationCodeBytes, false);
            String invalidationCodeHashString = Convert.toHexString(hashOfMessage);

            applicationResponse.put("invalidationCodeHashString", invalidationCodeHashString);

            synchronized (invalidationCache) {
                if (invalidationCache.containsKey(invalidationCodeHashString)) {
                    applicationResponse.put("isValid", false);
                    applicationResponse.put("invalidationCode", invalidationCodeHashString);
                    applicationResponse.put("age", Nxt.getEpochTime() - invalidationCache.get(invalidationCodeHashString));
                    applicationResponse.put("errorDescription",  "invalidation exists in invalidationCache");
                    return applicationResponse;
                }
            }

            if(configuration.adminPasswordString == null) {
                applicationResponse.put("isValid", false);
                applicationResponse.put("errorDescription",  "admin password not provided");
                return applicationResponse;
            }

            {
                long assetTransferAccountId = accountIdFromPublicKey(Crypto.getPublicKey(configuration.codeConfiguration.privateKey));

//            boolean codeIsInvalidated = existsDbPrunableMessage(assetTransferAccount.getId(), invalidationCodeHexString.getBytes(StandardCharsets.UTF_8));
                boolean codeIsInvalidated = searchForInvalidationMessageByHash(assetTransferAccountId, configuration.assetTransferChain, invalidationCodeHashString);

                if (codeIsInvalidated) {
                    applicationResponse.put("isValid", false);
                    applicationResponse.put("invalidationCode", invalidationCodeHashString);
                    applicationResponse.put("codeIsInvalidated", true);
                    applicationResponse.put("errorDescription",  "invalidation exists in blockchain");
                    return applicationResponse;
                }

                applicationResponse.put("isValid", true);
            }

            {
                int timestampCacheExpiry = Nxt.getEpochTime() - 24 * 60 * 60 * 2;

                synchronized (invalidationCache) {
                    invalidationCache.entrySet().removeIf(entry -> (entry.getValue() < timestampCacheExpiry));

                    if (broadcast) {
                        invalidationCache.put(invalidationCodeHashString, Nxt.getEpochTime());
                    }

                    applicationResponse.put("invalidationCode", invalidationCodeHexString);
                }
            }

            {
                HashMap<Long, Long> assetPickList = new HashMap<>();
                pickAssets(assetPickList, numberOfPicks, assetProbability, assetList);

                JSONArray jsonArray = new JSONArray();

                for (long asset : assetPickList.keySet()) {
                    Long quantityQNT = assetPickList.get(asset);

                    jsonArray.add(nxt.http.callers.TransferAssetCall.create(configuration.assetTransferChain)
                            .privateKey(configuration.codeConfiguration.privateKey)
                            .asset(asset)
                            .recipient(recipient)
                            .quantityQNT(quantityQNT)
                            .deadline(1440)
                            .broadcast(broadcast)
                            .message(invalidationCodeHexString).messageIsText(false).messageIsPrunable(true)
                            .feeRateNQTPerFXT(configuration.feeRateNQTPerFXT)
                            .call());
                }

                applicationResponse.put("transactions", jsonArray);
            }

            return applicationResponse;
        }

        private void updateConfigurationFeeRate(int chainId) {
            JA rates = nxt.http.callers.GetBundlerRatesCall.create()
                    .minBundlerBalanceFXT(10)
                    .minBundlerFeeLimitFQT(1)
                    .transactionPriority(configuration.feePriorityString)
                    .call()
                    .getArray("rates");

            for (JO rate : rates.objects()) {
                if (rate.getInt("chain") == chainId) {
                    configuration.feeRateNQTPerFXT = Convert.parseUnsignedLong(rate.getString("minRateNQTPerFXT"));

                    if (configuration.feeRateNQTPerFXT < configuration.minFeeRateNQTPerFXT)
                        configuration.feeRateNQTPerFXT = configuration.minFeeRateNQTPerFXT;

                    if (configuration.feeRateNQTPerFXT > configuration.maxFeeRateNQTPerFXT)
                        configuration.feeRateNQTPerFXT = configuration.maxFeeRateNQTPerFXT;
                }
            }

            applicationResponse.put("feeRateNQTPerFXT", configuration.feeRateNQTPerFXT);
        }

        private long codeVerifyBytes(byte[] bytes, CodeConfiguration codeConfiguration, MessageDigest messageDigest) {
            int metaDataSize = codeConfiguration.calculateSize();

            if(bytes == null || bytes.length < metaDataSize) {
                return -1;
            }

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

            // format : (magic[magic.length] : quantityQNT[0x08] : index[indexLength]) : signature[signatureLength]

            int bytesRead = 0;

            byte[] magic = new byte[codeConfiguration.magic.length];
            byte[] quantityQNTBytes = new byte[0x08];
            byte[] indexBytes = new byte[codeConfiguration.indexLength];
            byte[] signatureBytes = new byte[codeConfiguration.signatureLength];

            bytesRead += byteArrayInputStream.read(magic, 0x00, magic.length);
            bytesRead += byteArrayInputStream.read(quantityQNTBytes, 0x00, quantityQNTBytes.length);
            bytesRead += byteArrayInputStream.read(indexBytes, 0x00, indexBytes.length);
            bytesRead += byteArrayInputStream.read(signatureBytes, 0x00, signatureBytes.length);

            if(bytesRead != metaDataSize) {
                return -1;
            }

            byte[] signatureSource = new byte[codeConfiguration.signatureLength];
            byte[] codeBytes = new byte[metaDataSize - signatureSource.length];

            System.arraycopy(bytes, 0, codeBytes, 0, codeBytes.length);
            System.arraycopy(bytes, codeBytes.length, signatureSource, 0, signatureSource.length);

            byte[] signature512 = messageDigest.digest(codeBytes);
            byte[] signature = new byte[signatureBytes.length];

            System.arraycopy(signature512, 0, signature, 0, signature.length);

            long quantityQNT = -1;

            boolean isValid = Arrays.equals(signatureSource, signature);

            if(isValid) {
                quantityQNT = ByteBuffer.wrap(quantityQNTBytes).order(ByteOrder.BIG_ENDIAN).getLong();
            }

            return quantityQNT;
        }

        private void pickAssets(HashMap<Long, Long> assetPickList, long assetTransferTotal, List<Double> assetProbability, List<Long> assetList) {

            for(long index = 0; index < assetTransferTotal; index++) {
                double nextDouble = random.nextDouble();
                long asset = getRandomAsset(assetProbability, assetList, nextDouble);

                Logger.logInfoMessage("random : " + nextDouble + " : asset :  " + Long.toUnsignedString(asset));

                long quantity = (long) (1 * Math.pow(10, getAssetDecimals(asset)));

                if (assetPickList.containsKey(asset)) {
                    quantity += assetPickList.get(asset);
                }

                assetPickList.put(asset, quantity);
            }
        }

        private long getRandomAsset(List<Double> listProbability, List<Long> listAsset, double randomValue){
            long asset = 0;

            for(int index = 0; index < listProbability.size(); index++)
                if (listProbability.get(index) >= randomValue) {
                    asset = listAsset.get(index);
                    break;
                }

            return asset;
        }

        private void prepareRandom(byte[] bytesForRandom) {
            try {
               random = SecureRandom.getInstance("SHA1PRNG");
            } catch (NoSuchAlgorithmException e) {
                throw new IllegalArgumentException(e);
            }

            MessageDigest digest = Crypto.sha256();

            if(configuration.secretForRandomString != null) {
                byte[] secretForRandom = configuration.secretForRandomString.getBytes(StandardCharsets.UTF_8);
                digest.update(secretForRandom);
            }

            if(configuration.secretForRandomSerialString != null) {
                byte[] secretForRandomSerial = configuration.secretForRandomSerialString.getBytes(StandardCharsets.UTF_8);
                digest.update(secretForRandomSerial);
            }

            digest.update(bytesForRandom);

            long derivedSeedForAssetPick = ByteBuffer.wrap(digest.digest(), 0, 8).getLong();//bytesToLong(digest.digest());
            random.setSeed(derivedSeedForAssetPick);
        }

        private void generateCumulativeProbabilityFromAssetMap(List<Double> assetProbability, List<Long> assetList, TreeMap<Long, Double> assetMap, double assetTotal) {
            double probabilityCumulativeTotal = 0;

            for (Map.Entry<Long, Double> e : assetMap.entrySet()) {
                Long asset = e.getKey();
                Double quantity = e.getValue();

                double probability = quantity / assetTotal;
                probabilityCumulativeTotal += probability;

                assetProbability.add(probabilityCumulativeTotal);
                assetList.add(asset);
            }
        }

        private double assetCumulativeWeightListAppend(TreeMap<Long, Double> list, TreeMap<Long, Long> assetWeightMap) {

            int countOfAssets = assetWeightMap.size();

            if(countOfAssets == 0)
                return 0;

            double weightTotal = 0;

            for (long assetId: assetWeightMap.keySet()) {
                long assetWeight = assetWeightMap.get(assetId);

                double weight = assetWeight / Math.pow(10, getAssetDecimals(assetId));

                list.put(assetId, weight);
                weightTotal += weight;
            }

            return weightTotal;
        }

        private int getCurrentMinRollbackHeight() {
            JO response = nxt.http.callers.GetBlockchainStatusCall.create().call();

            return response.getInt("currentMinRollbackHeight");
        }

        private int getAssetDecimals(long assetId) {
            int assetDecimal = 0;

            boolean cached = false;

            synchronized (assetDecimalCache) {
                if(assetDecimalCache.containsKey(assetId)) {
                    cached = true;
                    assetDecimal = assetDecimalCache.get(assetId);
                }
            }

            if(!cached) {
                assetDecimal = nxt.http.callers.GetAssetCall.create().asset(assetId).call().getInt("decimals");

                synchronized (assetDecimalCache) {
                    assetDecimalCache.put(assetId, assetDecimal);
                }
            }

            return assetDecimal;
        }

        public static long accountIdFromPublicKey(byte[] publicKey) {
            MessageDigest md = null;

            try {
                md = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) { /* empty */ }

            assert md != null;
            byte[] publicKeyHash = md.digest(publicKey);

            return longLsbFromBytes(publicKeyHash);
        }

        public static long longLsbFromBytes(byte[] bytes) {
            BigInteger bi = new BigInteger(1, new byte[] {bytes[7], bytes[6], bytes[5], bytes[4], bytes[3], bytes[2], bytes[1], bytes[0]});
            return bi.longValue();
        }

        private byte[] messageHash(byte[] message, boolean isText) {
            MessageDigest messageDigest256;

            try {
                messageDigest256 = MessageDigest.getInstance("SHA-256");
            } catch (NoSuchAlgorithmException e) {
                return null;
            }

            byte[] messageHeader = new byte[1];

            if(isText) {
                messageHeader[0] = (byte) 0x01;
            }

            messageDigest256.update(messageHeader);
            return  messageDigest256.digest(message);
        }

        private boolean searchForInvalidationMessageByHash(long sender, int chainId, String invalidationMessageHashString) {

            synchronized (messageHashCache) {
                if(messageHashCache.contains(invalidationMessageHashString)) {
                    applicationResponse.put("messageHashCache", true);
                    return true;
                }

                JO response = nxt.http.callers.GetExecutedTransactionsCall.create(chainId)
                        .adminPassword(configuration.adminPasswordString)
                        .sender(sender)
                        .type(2).subtype(1)
                        .call();

                JA arrayOfTransactions = response.getArray("transactions");

                int minimumBlockHeight = getCurrentMinRollbackHeight();

                for (Object transactionObject : arrayOfTransactions) {
                    JO transactionJO = (JO) transactionObject;

                    JO attachment = transactionJO.getJo("attachment");

                    if (attachment == null)
                        continue;

                    if (attachment.getBoolean("messageIsText"))
                        continue;

                    if (!attachment.containsKey("messageHash")) {
                        continue;
                    }

                    int transactionBlockHeight = transactionJO.getInt("height");

                    String messageHash = attachment.getString("messageHash");

                    if (transactionBlockHeight < minimumBlockHeight) {
                        messageHashCache.add(messageHash);
                    }

                    if (!messageHash.equals(invalidationMessageHashString)) {
                        continue;
                    }

                    return true;
                }
            }

            return false;
        }

        private boolean existsDbPrunableMessage(long senderId, byte[] message) {
            Connection con = null;
            boolean result = false;

            try {
                con = Db.getConnection();

                String s = "SELECT sender_id FROM prunable_message WHERE sender_id = " +
                        senderId + " AND message = ? LIMIT 1";

                PreparedStatement pstmt = con.prepareStatement(s);

                pstmt.setBytes(1, message);

                ResultSet rs = pstmt.executeQuery();

                if (rs.next()) {
                    result = true;
                }

                con.close();

                return result;

            } catch (SQLException e) {
                DbUtils.close(con);
                throw new RuntimeException(e.toString(), e);
            }
        }

        @Override
        protected boolean requirePost() {
            return false;
        }

        @Override
        protected boolean requirePassword() {
            return false;
        }

        @Override
        protected boolean requireFullClient() {
            return true;
        }

        @Override
        protected boolean isChainSpecific() {
            return false;
        }
    }

    private static SortedSet<Long> setLongFromJsonStringArray(JSONArray jsonStringArray) {

        int jsonArraySize = jsonStringArray.size();

        if( jsonArraySize == 0)
            return null;

        SortedSet<Long> list = new TreeSet<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Long.parseUnsignedLong(jsonStringArray.get(i).toString()));
        }

        return list;
    }

    public static byte[] bytesFromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }

    public static class JsonSimpleFunctions {
        public static String getString(JSONObject jsonObject, String key, String defaultValue) {
            String result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (String) jsonObject.get(key);
                }
            }

            return result;
        }

        public static boolean getBoolean(JSONObject jsonObject, String key, boolean defaultValue) {
            boolean result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (boolean) jsonObject.get(key);
                }
            }

            return result;
        }

        public static int getInt(JSONObject jsonObject, String key, int defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (long) jsonObject.get(key);
                }
            }

            return (int) result;
        }

        public static long getLong(JSONObject jsonObject, String key, int defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = (long) jsonObject.get(key);
                }
            }

            return result;
        }

        public static long getLongFromStringUnsigned(JSONObject jsonObject, String key, long defaultValue) {
            long result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = Long.parseUnsignedLong((String) jsonObject.get(key));
                }
            }

            return result;
        }

        public static byte[] getBytesFromHexString(JSONObject jsonObject, String key, byte[] defaultValue) {
            byte[] result = defaultValue;

            if(jsonObject != null) {
                if(jsonObject.containsKey(key)) {
                    result = bytesFromHexString((String) jsonObject.get(key));
                }
            }

            return result;
        }

        public static byte[] bytesFromHexString(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];

            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
            }

            return data;
        }

        public static SortedSet<Long> setLongFromJsonStringArray(JSONArray jsonStringArray) {

            int jsonArraySize = jsonStringArray.size();

            if( jsonArraySize == 0)
                return null;

            SortedSet<Long> list = new TreeSet<>();

            for(int i = 0; i < jsonArraySize; i++) {
                list.add(Long.parseUnsignedLong(jsonStringArray.get(i).toString()));
            }

            return list;
        }

        public static TreeMap<Long, Long> mapLongLongFromJsonStringArray(JSONArray jsonStringArray) {

            int jsonArraySize = jsonStringArray.size();

            if( jsonArraySize == 0)
                return null;

            TreeMap<Long, Long> list = new TreeMap<>();

            for(int i = 0; i < jsonArraySize; i++) {
                JSONArray array = (JSONArray) jsonStringArray.get(i);
                list.put(Long.parseUnsignedLong(array.get(0).toString()), Long.parseUnsignedLong(array.get(1).toString()));
            }

            return list;
        }
    }
}
